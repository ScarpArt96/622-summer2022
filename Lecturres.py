# Exercises
#1.Write a Python program to count the number of strings where the string length is 2 or more and the first and last character are the same from a given list of strings
def compare(x):
    y = 0
    for i in x:
        if len(i)>2 and i[0] == i[-1]:
            y+=1
    return y
x =['abc','aba','1221','aaa','asdasdasa','baba']
for i in x:
    a=compare(x)
print(a)

#2.Write a Python program to remove duplicates from a list.
list1=[66, 4,'a','bc','bc','a','d', 4]
#print(set(list1))
list2 = []
for i in list1:
    if i not in list2:
        list2.append(i)
list1=list2
print(list1)

#3.Write a Python program to check a list is empty or not
my_list = []
if my_list==[]:
    print('The list is empty')
else:
    print(my_list)

#4.Write a Python program to find the index of an item in a specified list
data = [10, 15, 22, 35, 42, 50, 61, 72, 84]

def find_index(number,data,start,end):
    middle = (start + end)//2
    if number == data[middle]:
        return f"Number {number} found of index {middle}"
    elif number > data[middle]:
        return find_index(number,data,middle+1,end)
    else:
        return find_index(number,data,start,middle-1)
print(find_index(10,data,0,len(data)-1))

#5.Write a Python script to sort (ascending and descending) a dictionary by value

d = {'x': 3, 'y': 6, 'z': 4}
sorted_t ={key:value for key,value in sorted(d.items(),key=lambda item: item[1])}
print(sorted_t)

#5.1
y={'carl':40,'alan':2,'bob':1,'danny':3}
l=list(y.items())
l.sort()
print('Ascending order is',l)

l=list(y.items())
l.sort(reverse=True)
print('Descending order is',l)

dict=dict(l) # convert the list in dictionary

print("Dictionary",dict)

#6.Write a Python script to check whether a given key already exists in a dictionary
Adict = {'Mon':3,'Tue':5,'Wed':6,'Thu':9}
key_1=str(input('N'))#input the capital leatter
if key_1 in Adict:
    print(key_1, "is Present.")
else:
   print(key_1, " is not Present.")

#7.Write a Python program to remove a key from a dictionary.
dic= { 'A' : "John", 'B' : "Sam", 'C' : "Max" }
new_dic= { key:val for key, val in dic.items() if key!= 'B'}
print("Dictionary after removing key: ",new_dic)